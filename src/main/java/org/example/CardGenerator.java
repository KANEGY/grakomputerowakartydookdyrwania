package org.example;

import java.util.ArrayList;
import java.util.List;

public class CardGenerator {
    public static final List<Card> deckOfCards = generateCards();
    private static List<Card> generateCards() {
        List<Card> listOfCards = new ArrayList<>();
        List<String> highCards = new ArrayList<>();
        highCards.add("J");
        highCards.add("Q");
        highCards.add("K");
        highCards.add("A");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                switch (i) {
                    case 0 -> listOfCards.add(new Card(highCards.get(j) + "P"));
                    case 1 -> listOfCards.add(new Card(highCards.get(j) + "S"));
                    case 2 -> listOfCards.add(new Card(highCards.get(j) + "C"));
                    case 3 -> listOfCards.add(new Card(highCards.get(j) + "K"));
                }
            }
        }
        for (int j = 1; j <= 4; j++) {
            for (int i = 2; i <= 10; i++) {
                switch (j) {
                    case 1 -> listOfCards.add(new Card(i + "P"));
                    case 2 -> listOfCards.add(new Card(i + "S"));
                    case 3 -> listOfCards.add(new Card(i + "C"));
                    case 4 -> listOfCards.add(new Card(i + "K"));
                }
            }
        }
        return listOfCards;
    }
}
