package org.example;

import java.util.ArrayList;
import java.util.List;

public class Card {
    private String value;

    public Card(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



    @Override
    public String toString() {
        return "Card{" +
                "value='" + value + '\'' +
                '}';
    }
}
